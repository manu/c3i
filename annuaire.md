﻿% Coordonnées des membres de la C3i 
% Commission inter-IREM informatique

nom                 | lieu             | mail                                      | téléphone
--------------------|------------------|-------------------------------------------|---------------
Sylvie Alayrangues  | Poitiers         | <sylvie.alayrangues@univ-poitiers.fr>     |
Emmanuel Beffara    | Aix-Marseille    | <emmanuel.beffara@univ-amu.fr>            | 06 72 64 51 76
Xavier Buff         | Toulouse         | <xavier.buff@univ-tlse3.fr>               |
Sébastien Daniel    | Lorraine         | <sebastien.daniel@ac-nancy-metz.fr>       |
Christophe Declercq | Nantes           | <christophe.declercq@univ-nantes.fr>      |
Cédric Elophe       | Lorraine         | <cedric.elophe@ac-nancy-metz.fr>          | 06 72 09 34 42
Guillaume François  | Nantes           | <guillaume.francois@ac-nantes.fr>         |
Jean-Vincent Loddo  | Paris Nord       | <jean-vincent.loddo@lipn.univ-paris13.fr> |
Philippe Marquet    | Lille; SIF       | <Philippe.Marquet@univ-lille1.fr>         | 06 28 23 69 57
Antoine Meyer       | MLV              | <antoine.meyer@u-pem.fr>                  | 06 80 72 83 64
Malika More         | Clermont-Ferrand | <malika.more@udamail.fr>                  |
Nicolas Moreau      | Montpellier      | <nicolas.moreau@ac-montpellier.fr>        |
Florence Nény       | Marseille        | <florence.neny@ac-aix-marseille.fr>       |
Vincent Pantaloni   | Orléans          | <prof.pantaloni@gmail.com>                | 06 66 07 15 03
Gaëtan Perrin       | Clermont-Ferrand | <gaetan.perrin@ac-clermont.fr>            |
Cécile Prouteau     | Paris            | <cprouteau.irem.c3i@free.fr>              | 06 88 55 38 72
Sylviane Schwer     | Paris Nord       | <sylviane.schwer@lipn.univ-paris13.fr>    |
Philippe Truillet   | Toulouse         | <Philippe.Truillet@irit.fr>               | 06 87 27 58 79
Chloé Ubera         | Bordeaux         | <chloe.ubera@hotmail.fr>                  |
Stéphane Vinatier   | Limoges          | <stephane.vinatier@unilim.fr>             |
Jean-Marc Vincent   | Grenoble         | <Jean-Marc.Vincent@imag.fr>               | 06 51 32 95 91
