% Agenda de la c3i
% Commission inter-IREM informatique

# Réunions

## 7 octobre 2017

Réunion pour la création de la CII informatique.

## 8 et 9 décembre 2017

Première réunion.

## 16 et 17 mars 2018

Salles SG 1014 (vendredi 16) et HAF 264E (samedi 17).

Rendez-vous avec la CII Collège le vendredi après-midi (salle HAF 265E).

## 25 et 26 mai 2018

Salle HAF 234C (vendredi 25) HAF 264E (samedi 26).

En même temps que les CII Lycée et Université.

## Octobre 2018

À préciser pour prévoir un temps commun avec la CII didactique.


# Événements

- 15 au 17 janvier 2018:
  Réunion de la [Copirelem](http://www.univ-irem.fr/spip.php?rubrique12)
  à Cergy, avec atelier sur l'algorithmique.

- 26 et 27 janvier 2018:
  Journées des CII Lycée et Université à Limoges:
  *Avec l'informatique, des maths plus discrètes?*

- 7 au 9 février 2018:
  Conférence [Didapro Didastic](http://didapro.org/7/) à Lausanne.

- 12 au 14 juin 2018:
  [Colloque Copirelem](http://www.copirelem.fr/) à Blois:
  *Manipuler, représenter, communiquer: quelle place pour les artefacts dans
  l'enseignement et l'apprentissage des mathématiques?*

- 22 au 26 octobre 2018:
  Colloque [EMF 20180](https://emf2018.sciencesconf.org/) à Paris Gennevilliers.
